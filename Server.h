#pragma once
#pragma comment(lib, "ws2_32.lib")
#include <WinSock2.h>
#include <Windows.h>
#include <iostream>
#include <list>
#include <map>
#include <thread>
#include <string>
#include "Clients.h"
#include <exception>
#include <mutex>            
#include <condition_variable>


class Server
{
public:
	Server();
	~Server();
	void serve(int port);

private:

	std::list<std::thread> _threads;
	SOCKET _serverSocket;
	Clients _clients;
	std::mutex _mtx1;
	std::mutex _mtx2;
	std::condition_variable _cv;

	void accept();
	void clientHandler(SOCKET clientSocket);
	void handleMessage();
};

