#pragma once
#pragma comment(lib, "ws2_32.lib")
#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <vector>
#include <string>
#include <list>
#include <map>
#include <WinSock2.h>
#include <Windows.h>


class Clients
{
public:

	std::string connectedUsers();
	void addUser(std::string name);
	std::string getKey() const;
	void setKey(std::string key);
	void addMessage(std::string message);
	std::string getAllMessages();
	std::list<std::string> getNameList();
	void deleteName(std::string name);

private:

	std::map<std::string, std::string> _messages;
	std::list<std::string> _userNames;
	std::string _key;
};

