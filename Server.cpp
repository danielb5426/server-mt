#include "Server.h"
#include "Helper.h"
#include <fstream>
#include <chrono>

Server::Server()
{
	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (_serverSocket == INVALID_SOCKET)
	{
		throw std::exception(__FUNCTION__ " - socket");
	}
}

Server::~Server()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		closesocket(_serverSocket);
	}
	catch (...) {}
}

void Server::serve(int port)
{
	
	struct sockaddr_in sa = { 0 };
	
	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (bind(_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");
	
	// Start listening for incoming requests of clients
	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening on port " << port << std::endl;
	std::thread t(&Server::handleMessage, this);
	t.detach();
	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		std::cout << "Waiting for client connection request" << std::endl;
		accept();
	}
}


void Server::accept()
{
	// notice that we step out to the global namespace
	// for the resolution of the function accept

	// this accepts the client and create a specific socket from server to this client
	SOCKET client_socket = ::accept(_serverSocket, NULL, NULL);
	
	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	std::cout << "Client accepted. Server and client can speak" << std::endl;

	// the function that handle the conversation with the client
	_threads.push_back(std::thread(&Server::clientHandler, this, client_socket));
	_threads.back().detach();
}

void Server::clientHandler(SOCKET clientSocket)
{
	std::string name1;
	try
	{
		std::string name, secondeName, message, fileContent, path;
		char* m = Helper::getDataFromSocket(clientSocket);
		int lenName = Helper::getIntPartFromSocket(m, 3, 2), lenMessage = 0;
		// build first update message and save user
		name = Helper::getPartFromSocket(m, 5, lenName);
		name1 = name;
		_clients.addUser(name);
		std::cout << _clients.connectedUsers() << std::endl;
		Helper::send_update_message_to_client(clientSocket, "", "", _clients.connectedUsers());
		while (true)
		{
			m = Helper::getDataFromSocket(clientSocket);
			lenName = Helper::getIntPartFromSocket(m, 3, 2);
			secondeName = Helper::getPartFromSocket(m, 5, lenName);
			lenMessage = Helper::getIntPartFromSocket(m, 5 + lenName, 5);
			message = Helper::getPartFromSocket(m, 10 + lenName, lenMessage);
			fileContent = "";
			if (name < secondeName)
			{
				path = name + "&" + secondeName + ".txt";
			}
			else
			{
				path = secondeName + "&" + name + ".txt";
			}
			_mtx2.lock();
			_clients.setKey(path);
			if (message != "")
			{
				std::unique_lock<std::mutex> lock(_mtx1); // lock
				_clients.addMessage("&MAGSH_MESSAGE&&Author&" + name + "&DATA&" + message);
				lock.unlock();// unlock
				_cv.notify_one();
			}
			_mtx2.unlock();
			fileContent = _clients.getAllMessages();
			
			Helper::send_update_message_to_client(clientSocket, fileContent, secondeName, _clients.connectedUsers());
		}

		// Closing the socket (in the level of the TCP protocol)
		closesocket(clientSocket);
	}
	catch (const std::exception& e)
	{
		closesocket(clientSocket);
		_clients.deleteName(name1);
	}
}

void Server::handleMessage()
{
	std::ofstream chat;
	while (1)
	{
		std::unique_lock<std::mutex> lock(_mtx1);
		_cv.wait(lock);
		chat.open(_clients.getKey(), std::ofstream::out | std::ofstream::trunc);
		chat.close();
		chat.open(_clients.getKey());
		chat << _clients.getAllMessages();
		chat.close();
		lock.unlock();
	}
}




