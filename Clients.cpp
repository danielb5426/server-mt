#include "Clients.h"


std::string Clients::connectedUsers()
{
	std::string userList;
	for (auto const& i : _userNames)
	{
		userList += i;
		userList += "&";
	}

	userList = userList.substr(0, userList.size() - 1);
	return userList;
}

void Clients::addUser(std::string name)
{
	_userNames.push_back(name);
}

std::string Clients::getKey() const
{
	return _key;
}

void Clients::setKey(std::string key)
{
	_key = key;
}

void Clients::addMessage(std::string message)
{
	if (_messages.find(_key) == _messages.end())
	{
		_messages.insert({ _key, message });// for the first time
	}
	else
	{
		_messages[_key] += message;
	}
}

std::string Clients::getAllMessages()
{
	return _messages[_key];
}

std::list<std::string> Clients::getNameList()
{
	return _userNames;
}

void Clients::deleteName(std::string name)
{
	_userNames.remove(name);
}
